app_dir := $(dir $(CURDIR))

all: container

container: 
	sudo docker build -t simple-sbcl .

run: container
	sudo docker run -a stdin -a stdout -a stderr -i -t simple-sbcl

clean:
	sudo docker rm $(docker ps -a -q)
	sudo docker rmi $(docker images -q)
