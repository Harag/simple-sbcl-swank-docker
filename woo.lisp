;; Runs a woo http server listening on port 500
(woo:run 
 (lambda (env)
   ;;The cond just shows some simple routing and output.
   (cond ((equalp (getf env :REQUEST-URI) "/test")
	  (list 200
		(list :content-type "text/plain")
		(list (format nil "Hello, World - ~A" (getf env :REQUEST-URI)))))
	 (t
	  (list 200
		(list :content-type "text/plain")
		(list (format nil "~S" env))))))
 ;;For docker you need to bind to 0.0.0.0 and not 127.0.0.1
 ;;google it for more info
 :address "0.0.0.0"
 :port 5000)
