# simple-sbcl-swank-docker

There are a lot of sbcl, swank dockers out there but they all have different base docker images, some of those doing complicated stuff.
This repo is to show how to do sbcl and swank of a clean  phusion/baseimage

See heavily commented Dockerfile for what this repo is about

## Testing locally

```cd [source folder]```

To build cd to the source directory

```sudo docker build --tag simple-sbcl:1.0 .```

To run

```sudo docker run -ti --publish 5000:5000 --publish 4005:4005  --name simple-sbcl simple-sbcl:1.0```

Now you can go to http://localhost:5000 to check out what woo is up to.

In emacs slime-connect to localhost port 4005.


To git rid of then running docker

```sudo docker rm --force simple-sbcl.```
